import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.scss']
})
export class DesignComponent implements OnInit {
  showHide:boolean=true;
  constructor(public data:DataService) { }

  ngOnInit(): void {
    this.data.getSidebar().subscribe(status=> {
      console.log(status)
      this.showHide=status;
    })
  }

}
