import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { DesignComponent } from './design/design.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [DesignComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    SharedModule
  ]
})
export class LayoutModule { }
