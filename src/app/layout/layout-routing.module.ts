import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesignComponent } from './design/design.component';

const routes: Routes = [
  {
    path: '',
    component:DesignComponent,
    children: [
      {
          path: "",
          loadChildren: () => import(`../modules/modules.module`).then(m => m.ModulesModule)

      }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
