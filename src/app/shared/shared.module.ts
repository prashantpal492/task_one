import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinegraphComponent } from './linegraph/linegraph.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TableComponent } from './table/table.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [LinegraphComponent, HeaderComponent, SidebarComponent, TableComponent],
  imports: [
    CommonModule,
  ],
  exports: [
    LinegraphComponent,
    HeaderComponent,
    SidebarComponent,
    TableComponent,
    RouterModule
  ]
})
export class SharedModule { }
