import { Component, Input, OnInit } from '@angular/core';

import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'app-linegraph',
  templateUrl: './linegraph.component.html',
  styleUrls: ['./linegraph.component.scss']
})
export class LinegraphComponent implements OnInit {

  @Input() id;
  @Input() Graphdata;
  constructor(private data: DataService) { }
   ngOnInit(): void {
    this.data.getchartDetail().subscribe(res=>{
      console.log(res)
      this.Graphdata=res;
    })
    setTimeout(() => {
      this.linechartBooking()
    }, 100)
  }

  linechartBooking() {
    console.log(this.id)
// Create chart instance
let chart = am4core.create(this.id, am4charts.XYChart);

// Add data
chart.data = this.Graphdata

// Create axes
let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.minGridDistance = 50;

let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
let series = chart.series.push(new am4charts.LineSeries());
series.dataFields.valueY = "current";
series.dataFields.dateX = "date";
series.strokeWidth = 2;
series.minBulletDistance = 10;
series.tooltipText = "[bold]current : {current}";
series.tooltip.pointerOrientation = "vertical";
series.tensionX = 0.8;

// Create series
let series2 = chart.series.push(new am4charts.LineSeries());
series2.dataFields.valueY = "upcoming";
series2.dataFields.dateX = "date";
series2.strokeWidth = 2;
series.minBulletDistance = 10;
series2.tooltipText = "[bold]upcoming : {upcoming}";
series2.strokeWidth = 2;
series2.tensionX = 0.8;

let series3 = chart.series.push(new am4charts.LineSeries());
series3.dataFields.valueY = "upcoming";
series3.dataFields.dateX = "date";
series3.strokeWidth = 2;
series3.minBulletDistance = 10;
series3.tooltipText = "[bold]past : {past}";
series3.strokeWidth = 2;
series3.tensionX = 0.8;

let series4 = chart.series.push(new am4charts.LineSeries());
series4.dataFields.valueY = "upcoming";
series4.dataFields.dateX = "date";
series4.strokeWidth = 2;
series4.minBulletDistance = 10;
series4.tooltipText = "[bold]cancellation : {cancellation}";
series4.strokeWidth = 2;
series4.tensionX = 0.8;
// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = dateAxis;
var scrollbarX = new am4core.Scrollbar();
scrollbarX.marginBottom = 20;
chart.scrollbarX = scrollbarX;
  }

 

}
