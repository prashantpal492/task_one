import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  dataValue:any;
  bookingId="id"
  dataChart = [
    {date:new Date(2019,5,12), current:50, upcoming:48, past:48, cancellation:48},
    {date:new Date(2019,5,13), current:53, upcoming:51, past:51, cancellation:33},
    {date:new Date(2019,5,14), current:56, upcoming:58, past:58, cancellation:44},
    {date:new Date(2019,5,15), current:52, upcoming:53, past:53, cancellation:22},
    {date:new Date(2019,5,16), current:48, upcoming:44, past:44, cancellation:33},
    {date:new Date(2019,5,17), current:47, upcoming:42, past:48, cancellation:48},
    {date:new Date(2019,5,18), current:59, upcoming:55, past:48, cancellation:48}
  ]
  constructor(private data: DataService) { }

  ngOnInit(): void {
    this.dataValue=this.data.data;
    console.log
    this.data.setchartDetail(this.dataValue.chartData)
    console.log(this.dataValue)
  }
  
}
