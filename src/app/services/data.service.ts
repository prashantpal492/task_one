import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private sidebar= new BehaviorSubject<boolean>(true);
  private chartDetail= new BehaviorSubject<any>([]);
  constructor() { }
  setSidebar(details: any) {
    this.sidebar.next(details);
  }

  getSidebar() {
    return this.sidebar.asObservable();
  }
  setchartDetail(details: any) {
    this.chartDetail.next(details);
  }

  getchartDetail() {
    return this.chartDetail.asObservable();
  }

  data=[
    {
      product_id: 1,
      product_name: 'Product-1',
      product_total_quantity: 40,
      product_sales: 15,
      product_remaining_quantity:25,
      price: '20',
      chartData: [
        {date:new Date(2019,5,12), current:50, upcoming:48, past:48, cancellation:48},
        {date:new Date(2019,5,13), current:53, upcoming:51, past:51, cancellation:33},
        {date:new Date(2019,5,14), current:56, upcoming:58, past:58, cancellation:44},
        {date:new Date(2019,5,15), current:52, upcoming:53, past:53, cancellation:22},
        {date:new Date(2019,5,16), current:48, upcoming:44, past:44, cancellation:33},
        {date:new Date(2019,5,17), current:47, upcoming:42, past:48, cancellation:48},
        {date:new Date(2019,5,18), current:59, upcoming:55, past:48, cancellation:48}
      ]
    },
    {
      product_id: 2,
      product_name: 'Product-2',
      product_total_quantity: 50,
      product_sales: 25,
      product_remaining_quantity:25,
      price: '30',
      chartData: [
        {date:new Date(2019,5,12), current:50, upcoming:48, past:48, cancellation:48},
        {date:new Date(2019,5,13), current:53, upcoming:51, past:51, cancellation:33},
        {date:new Date(2019,5,14), current:56, upcoming:58, past:58, cancellation:44},
        {date:new Date(2019,5,15), current:52, upcoming:53, past:53, cancellation:22},
        {date:new Date(2019,5,16), current:48, upcoming:44, past:44, cancellation:33},
        {date:new Date(2019,5,17), current:47, upcoming:42, past:48, cancellation:48},
        {date:new Date(2019,5,18), current:59, upcoming:55, past:48, cancellation:48}
      ]
      
    },
    {
      product_id: 1,
      product_name: 'Product-3',
      product_total_quantity: 40,
      product_sales: 15,
      product_remaining_quantity:25,
      price: '20',
      chartData: [
        {date:new Date(2019,5,12), current:50, upcoming:48, past:48, cancellation:48},
        {date:new Date(2019,5,13), current:53, upcoming:51, past:51, cancellation:33},
        {date:new Date(2019,5,14), current:56, upcoming:58, past:58, cancellation:44},
        {date:new Date(2019,5,15), current:52, upcoming:53, past:53, cancellation:22},
        {date:new Date(2019,5,16), current:48, upcoming:44, past:44, cancellation:33},
        {date:new Date(2019,5,17), current:47, upcoming:42, past:48, cancellation:48},
        {date:new Date(2019,5,18), current:59, upcoming:55, past:48, cancellation:48}
      ]
    },{
      product_id: 1,
      product_name: 'Product-4',
      product_total_quantity: 40,
      product_sales: 15,
      product_remaining_quantity:25,
      price: '20',
      chartData: [
        {date:new Date(2019,5,12), current:50, upcoming:48, past:48, cancellation:48},
        {date:new Date(2019,5,13), current:53, upcoming:51, past:51, cancellation:33},
        {date:new Date(2019,5,14), current:56, upcoming:58, past:58, cancellation:44},
        {date:new Date(2019,5,15), current:52, upcoming:53, past:53, cancellation:22},
        {date:new Date(2019,5,16), current:48, upcoming:44, past:44, cancellation:33},
        {date:new Date(2019,5,17), current:47, upcoming:42, past:48, cancellation:48},
        {date:new Date(2019,5,18), current:59, upcoming:55, past:48, cancellation:48}
      ]
    }
  ]
}
